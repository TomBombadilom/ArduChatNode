/* <reference path="../node_modules/primeng/primeng.d.ts" /> */
import {Component, ViewEncapsulation} from '@angular/core';
import {ConfirmationService, Message} from "primeng/components/common/api";
import {MenuItem} from 'primeng/api';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  providers:  [ConfirmationService],
  /* encapsulation: ViewEncapsulation.Native   // Native is not supported  */
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'Chatière Ardu Node';
  name: string;
  userResponse: Message[]=[];
  theUserSaid: string;
  private items: MenuItem[];
  ngOnInit() {
        this.items = [
            {
                label: 'Arduino',
                items: [{
                        label: 'New',
                        icon: 'fa-plus',
                        items: [
                            {label: 'Chatière'},
                            {label: 'Chat'},
                        ]
                    },
                    {label: 'Configure'},
                    {label: 'Connection test'},
                    {label: 'Quit'}
                ]
            },
            {
                label: 'Edit',
                icon: 'fa-edit',
                items: [
                    {label: 'Undo', icon: 'fa-mail-forward'},
                    {label: 'Redo', icon: 'fa-mail-reply'}
                ]
            }
        ];
	}


	constructor(private confirmationService: ConfirmationService) {}

    onChangeEvent({target}){
        this.name = target.value;
        console.log(this.name);
    }

    greetMe(){

        this.confirmationService.confirm({
            message: ` Hey ${this.name}, do you like Chatière Ardu Node ?`,
            header: 'ça vous plait ?',
            icon: 'fa fa-question-circle',
            accept: () => {
                this.userResponse = [];
                this.userResponse.push({severity:'info', summary:'Confirmed', detail:'I love it'});
                this.theUserSaid = this.name + " responded " + this.userResponse[0].detail;
            },
            reject: () => {
                this.userResponse = [];
                this.userResponse.push({severity:'info', summary:'Rejected', detail:'I don\'t really like Chatière Ardu Node :\( '});
                this.theUserSaid = this.name + " responded " + this.userResponse[0].detail;
            }
        });
    } 
}
