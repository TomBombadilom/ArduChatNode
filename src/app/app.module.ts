import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { InputTextModule, ConfirmDialogModule } from 'primeng/primeng';
import { AccordionModule } from 'primeng/primeng';
import { PanelModule } from 'primeng/primeng';
import { ButtonModule } from 'primeng/primeng';
import { RadioButtonModule } from 'primeng/primeng';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { MenubarModule } from 'primeng/menubar';
import { AppComponent } from './app.component';


@NgModule({
  imports: [
   BrowserModule,
   BrowserAnimationsModule,
   InputTextModule,
   ButtonModule,
   RadioButtonModule,  
   ConfirmDialogModule,
   AccordionModule,
   PanelModule,
   FormsModule,
   MenubarModule
   ],
  providers: [],  
  declarations: [ AppComponent ],  
  bootstrap: [ AppComponent ]
})
export class AppModule { 
}
