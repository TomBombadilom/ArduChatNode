# How to install on linux

## Install nodejs and npm >=9
for other systems please read 
https://nodejs.org/en/download/package-manager/

### Ubuntu 
curl -sL https://deb.nodesource.com/setup_9.x | sudo -E bash -
sudo apt-get install -y nodejs

### Debian 
curl -sL https://deb.nodesource.com/setup_9.x | sudo -E bash -
sudo apt-get install -y nodejs

Optional: install build tools

To compile and install native addons from npm you may also need to install build tools:

sudo apt-get install -y build-essential

### Archlinux
Node.js and npm packages are available in the Community Repository.

pacman -S nodejs npm

or yaourt -S nodejs npm 

## Install Angular for dev purpose only 
please read :

https://angular.io/guide/quickstart

Then install the Angular CLI globally.

npm install -g @angular/cli

# Quick Install of ArduChatNode 

## clone ArduChatNode from master branch
git clone git@framagit.org:TomBombadilom/ArduChatNode.git

## Install all dependencies used by the program 
cd ArduChatNode

npm install 

nb: see https://framagit.org/TomBombadilom/ArduChatNode/blob/master/.angular-cli.json for all dependancies needed by angular client

nb2: see https://framagit.org/TomBombadilom/ArduChatNode/blob/master/package.json far all dependencies and command that you can configure and update as you wish 

nb3: for all changes in this files please npm install again to download all the libraries needed 

nb 4 ./start.sh is just a way for dev to lauch the nodejs server , you can also use the command ng serve --open --port xxxxx instead  
